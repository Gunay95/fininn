import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {User} from "../../models/user";
import {environment} from "../../environments/environment";
import {HttpClientProvider} from "../http-client/http-client";

@Injectable()
export class SecurityProvider {

  constructor(public http: HttpClient, public httpClient: HttpClientProvider) {
    console.log('Hello SecurityProvider Provider');
  }

  auth(user: User) {
    return this.httpClient.post(environment.URLS.LOGIN, user);
  }


  register(user: User) {
    const u = <User> JSON.parse(JSON.stringify(user));
    return this.httpClient.post(environment.URLS.REGISTER, u);
  }

}
