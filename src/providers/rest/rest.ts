import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {HttpClientProvider} from "../http-client/http-client";
import {UtilProvider} from "../util/util";
import {User} from "../../models/user";
import {environment} from "../../environments/environment";

@Injectable()
export class RestProvider {

    constructor(public http: HttpClient, private httpClient: HttpClientProvider, private utilService: UtilProvider) {
        console.log('Hello RestProvider Provider');
    }

    getDepositList() {
        return this.httpClient.get(environment.URLS.DEPOSIT);
    }

    getDepositPeriods() {
        return this.httpClient.get(environment.URLS.DEPOSIT_PERIODS);
    }

    addDeposit(body) {
        return this.httpClient.post(environment.URLS.DEPOSIT, body);
    }

  getTransactionList(id) {
    return this.httpClient.get(environment.URLS.TRANSACTION_LIST.replace(':id', id));
  }

  updateUserAccount(body) {
    return this.httpClient.put(environment.URLS.UPDATE_USER_ACCOUNT, body);
  }

  getUserAccount() {
    return this.httpClient.get(environment.URLS.UPDATE_USER_ACCOUNT);
  }

}
