import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoadingController, ToastController} from "ionic-angular";

@Injectable()
export class UtilProvider {
  loading;

  constructor(public http: HttpClient, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    console.log('Hello UtilProvider Provider');
  }

  checkValueByPattern(value, pattern) {
    const email = new FormGroup({
      username: new FormControl(value, Validators.pattern(pattern))
    });
    return email.valid
  }

  showLoading() {
    let content = 'Please wait...';
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: content
    });

    this.loading.present();

  }

  hideLoading() {
    setTimeout(() => {
      if (this.loading) {
        this.loading.dismiss();
        this.loading = null;
      }
    }, 1000);
  }

  presentToast(position: string, message: string, duration: number) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present(toast);
  }

}
