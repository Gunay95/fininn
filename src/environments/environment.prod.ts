export const API_URL = 'http://10.10.10.14:8085'; //'http://172.20.10.40:81/restapi';

export const environment = {
  production: false,
  i18nPath: '/assets/i18n/',
  authKey: 'Authorization',


  URLS: {
    LOGIN: API_URL + '/public/auth/login/',
    REGISTER: API_URL + '/public/auth/register',
    DEPOSIT: API_URL + '/private/deposit',
    DEPOSIT_PERIODS: API_URL + '/private/depositPeriod',
    TRANSACTION_LIST: API_URL + '/private/deposit/:id',
    UPDATE_USER_ACCOUNT: API_URL + '/private/user'

  }
};
