import {DepositPeriod} from "./deposit-period";

export class Deposit {
    amount: number;
    id: number;
    idUser: number;
    depositPeriodResponse: DepositPeriod;
    insertDate: number;

    constructor() {}
}