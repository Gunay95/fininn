export class User {
  username: string;
  phone: string;
  password: string;
  name: string;
  surname: string;

  patterns = {
    username: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    password: /^(?=.*\d)(\S{6,30})$/,
    name: /^[üÜöÖğĞəƏıIİşŞçÇa-zA-Zа-яА-Я]+$/,
    surname: /^[üÜöÖğĞəƏıIİşŞçÇa-zA-Zа-яА-Я]+$/,
    phoneNumber: /^\+{0,1}\d{9,12}$/
  };

  constructor() {
  }

}
