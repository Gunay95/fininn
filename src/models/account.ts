export class Account {
  fin:	string;
  name:	string;
  surname:	string;
  phone:	string;
  serial:	string;
  email: string;

  patterns = {
    name: /^[üÜöÖğĞəƏıIİşŞçÇa-zA-Zа-яА-Я]+$/,
    surname: /^[üÜöÖğĞəƏıIİşŞçÇa-zA-Zа-яА-Я]+$/,
    phoneNumber: /^\+{0,1}\d{9,12}$/
  };

  constructor() {
  }

}
