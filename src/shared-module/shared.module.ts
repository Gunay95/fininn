import {NgModule} from "@angular/core";
import {FabButtonPage} from "../pages/fab-button/fab-button";
import {IonicPageModule} from "ionic-angular";

@NgModule({
    declarations: [
        FabButtonPage
    ],
    imports: [
        IonicPageModule.forChild(FabButtonPage)
    ],
    exports: [
        FabButtonPage
    ]
})

export class SharedModule {}