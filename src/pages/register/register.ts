import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user";
import {SecurityProvider} from "../../providers/security/security";
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
  name: 'register'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {
  user: User = new User();
  registerForm: FormGroup;
  errorMessage: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private securityService: SecurityProvider, private utilService: UtilProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      phone: ['', new FormControl()],
      name: ['', Validators.compose([Validators.required])],
      surname: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  get controls() {
    return this.registerForm.controls;
  }

  register() {
    this.registerForm = this.formBuilder.group({
      username: [this.user.username, Validators.compose([Validators.required, Validators.pattern(this.user.patterns.username)])],
      phone: [this.user.phone, new FormControl()],
      name: [this.user.name, Validators.compose([Validators.required, Validators.pattern(this.user.patterns.name)])],
      surname: [this.user.surname, Validators.compose([Validators.required, Validators.pattern(this.user.patterns.surname)])],
      password: [this.user.password, Validators.compose([Validators.required])],
    });
    if (this.registerForm.invalid) {
      return;
    }
    this.utilService.showLoading();
    this.securityService.register(this.user).subscribe(
      (result: any) => {
        const securityResponse: any = result.data;
        this.utilService.hideLoading();
        if (result.status.code > 200) {
          this.utilService.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        localStorage.setItem('token', securityResponse.token);
        this.navCtrl.setRoot('main');
      }, (error) => {
        this.utilService.hideLoading();
      });
  }

  closeRegisterPage() {
    this.navCtrl.pop();
  }

  signIn() {
    this.navCtrl.pop()
  }

  errorFocusOut($event) {
    if (!this.utilService.checkValueByPattern(this.user.name, this.user.patterns.name)) {
      this.errorMessage = 'name';
    } else if (!this.utilService.checkValueByPattern(this.user.surname, this.user.patterns.surname)) {
      this.errorMessage = 'surname';
    } else if (!this.utilService.checkValueByPattern(this.user.username, this.user.patterns.username)) {
      this.errorMessage = 'username';
    } else if (!this.utilService.checkValueByPattern(this.user.phone, this.user.patterns.phoneNumber)) {
      this.errorMessage = 'phoneNumber';
    } else if (!this.utilService.checkValueByPattern(this.user.password, this.user.patterns.password)) {
      this.errorMessage = 'password';
    } else {
      this.errorMessage = '';
    }

    console.log(this.errorMessage);
  }


  signin() {
    this.navCtrl.push('login');
  }

}
