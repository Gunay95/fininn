import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import {RestProvider} from "../../providers/rest/rest";
import {SecurityProvider} from "../../providers/security/security";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {HttpClientModule} from "@angular/common/http";
import {UtilProvider} from "../../providers/util/util";

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    HttpClientModule,
  ],
  providers: [
    RestProvider,
    SecurityProvider,
    HttpClientProvider,
    UtilProvider,
  ],
})
export class RegisterPageModule {}
