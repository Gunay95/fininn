import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AddDepositPage} from './add-deposit';
import {RestProvider} from "../../providers/rest/rest";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";
import {SharedModule} from "../../shared-module/shared.module";

@NgModule({
    declarations: [
        AddDepositPage
    ],
    imports: [
        IonicPageModule.forChild(AddDepositPage),
        SharedModule
    ],
    providers: [RestProvider, HttpClientProvider, UtilProvider]
})
export class AddDepositPageModule {
}
