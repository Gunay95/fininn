import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";
import {DepositPeriod} from "../../models/deposit-period";
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
  name: 'add-deposit'
})
@Component({
  selector: 'page-add-deposit',
  templateUrl: 'add-deposit.html',
})
export class AddDepositPage {
  amount = 100;
  currency = 'AZN';
  period = 1;
  percent = '0';
  monthlyInterest = 0;
  endOfPeriodInterest = 0;
  totalAmount = 0;
  selectedDepositPeriod;
  depositId;

  depositPeriods: DepositPeriod[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private rest: RestProvider, private util: UtilProvider, private utilService: UtilProvider) {
  }

  ionViewWillEnter() {
    this.getDepositPeriods();
  }

  ionViewDidLoad() {
    this.depositId = this.navParams.get('depositId');
  }

  nextClick() {
    const data = {
      amount: this.amount,
      idDepositPeriod: this.selectedDepositPeriod.id,
      id: this.depositId,
      currency: this.currency
    };
    this.navCtrl.push('payment', {data: data});
  }

  getDepositPeriods() {
    this.util.showLoading();
    this.rest.getDepositPeriods().subscribe(
      (result: any) => {
        this.util.hideLoading();
        if (result.status.code > 200) {
          this.utilService.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        this.depositPeriods = result.data;
      }, (error) => {
        this.utilService.hideLoading();
      });
  }

  calculateDeposit() {
    const depositPeriod = this.depositPeriods.find((dp) => dp.period === +this.period && dp.currency.toLowerCase() === this.currency.toLowerCase());
    console.log(depositPeriod)
    if (depositPeriod) {
      this.selectedDepositPeriod = depositPeriod;
      this.percent = depositPeriod.percent.toString();
    }
    this.monthlyInterest = +(this.amount * (depositPeriod.percent / depositPeriod.period) / 100).toFixed(2);
    this.endOfPeriodInterest = +(this.amount * depositPeriod.percent / 100).toFixed(2);
    this.totalAmount = this.amount + this.endOfPeriodInterest;
  }

}
