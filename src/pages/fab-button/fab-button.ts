import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-fab-button',
    templateUrl: 'fab-button.html',
})
export class FabButtonPage {

    openMenu = false;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    togglePopupMenu() {
        return this.openMenu = !this.openMenu;
    }

    goToPage(pagename?) {
        if (pagename == 'log-out') {
          localStorage.removeItem('token');
          this.navCtrl.setRoot('login');
        } else if (pagename) {
            this.navCtrl.setRoot(pagename);
        }
        this.togglePopupMenu();
    }

}
