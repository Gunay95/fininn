import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FabButtonPage } from './fab-button';

@NgModule({
  declarations: [
    FabButtonPage,
  ],
  imports: [
    IonicPageModule.forChild(FabButtonPage),
  ],
})
export class FabButtonPageModule {}
