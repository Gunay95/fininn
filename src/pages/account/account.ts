import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";
import {Account} from "../../models/account";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
  name: 'account'
})
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage implements OnInit {
  account: Account = new Account();
  accountForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private rest: RestProvider, private utilService: UtilProvider, private formBuilder: FormBuilder) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  ngOnInit() {
    this.getUserInfo();
    this.accountForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern(this.account.patterns.name)])],
      surname: ['', Validators.compose([Validators.required, Validators.pattern(this.account.patterns.surname)])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern(this.account.patterns.phoneNumber)])],
      fin: ['', Validators.compose([Validators.required])],
      serial: ['', Validators.compose([Validators.required])],
    });
  }

  saveClick() {
    console.log(this.account)
    this.accountForm = this.formBuilder.group({
      name: [this.account.name, Validators.compose([Validators.required, Validators.pattern(this.account.patterns.name)])],
      surname: [this.account.surname, Validators.compose([Validators.required, Validators.pattern(this.account.patterns.surname)])],
      phone: [this.account.phone, new FormControl()],
      fin: [this.account.fin, Validators.compose([Validators.required])],
      serial: [this.account.serial, Validators.compose([Validators.required])],
    });
    if (this.accountForm.invalid) {
      return;
    }
    const fin = this.account.fin;
    const name = this.account.name;
    const phone = this.account.phone;
    const serial = this.account.serial;
    const surname = this.account.surname;
    const body = {fin, name, phone, serial, surname};
    this.utilService.showLoading();
    this.rest.updateUserAccount(body).subscribe(
      data => {
        const result: any = data;
        this.utilService.hideLoading();
        if (result.status.code > 200) {
          this.utilService.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        if (result.status.message === 'ok') {
          this.utilService.presentToast('bottom', 'Your information successfully update', 3000);
        }
      }, (error) => {
        this.utilService.hideLoading();
      });
  }

  getUserInfo() {
    this.utilService.showLoading();
    this.rest.getUserAccount().subscribe(
      (result: any) => {
        this.utilService.hideLoading();
        if (result.status.code > 200) {
          this.utilService.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        this.account.email = result.data.email;
        this.account.phone = result.data.phone;
        this.account.fin = result.data.fin;
        this.account.serial = result.data.serial;
        this.account.surname = result.data.surname;
        this.account.name = result.data.name;
      }, (error) => {
        this.utilService.hideLoading();
      });
  }
}
