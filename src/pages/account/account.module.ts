import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import {SharedModule} from "../../shared-module/shared.module";
import {RestProvider} from "../../providers/rest/rest";
import {SecurityProvider} from "../../providers/security/security";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";

@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountPage),
    SharedModule
  ],
  providers: [
    RestProvider,
    SecurityProvider,
    HttpClientProvider,
    UtilProvider,
  ],
})
export class AccountPageModule {}
