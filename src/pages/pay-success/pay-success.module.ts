import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaySuccessPage } from './pay-success';
import {SharedModule} from "../../shared-module/shared.module";

@NgModule({
  declarations: [
    PaySuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(PaySuccessPage),
      SharedModule
  ],
})
export class PaySuccessPageModule {}
