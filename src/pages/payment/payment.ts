import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CardIO} from "@ionic-native/card-io";
import {RestProvider} from "../../providers/rest/rest";


@IonicPage({
    name: 'payment'
})
@Component({
    selector: 'page-payment',
    templateUrl: 'payment.html',
})
export class PaymentPage {

    cardImage = 'assets/imgs/card.svg';
    card = {
        cardType: '',
        cardNumber: '',
        redactedCardNumber: '',
        expiryMonth: null,
        expiryYear: null,
        cvv: '',
        postalCode: ''
    };
    deposit = {amount: 0, id: 0, idDepositPeriod: 0};

    constructor(public navCtrl: NavController, public navParams: NavParams, public cardIO: CardIO, private rest: RestProvider) {
    }

    ionViewDidLoad() {
        this.deposit = this.navParams.get('data');
    }


    scanCard() {
        this.cardIO.canScan()
            .then(
                (res: boolean) => {
                    if (res) {
                        const options = {
                            scanExpiry: true,
                            hideCardIOLogo: true,
                            scanInstructions: 'Please position your card inside the frame',
                            keepApplicationTheme: true,
                            requireCCV: true,
                            requireExpiry: true,
                            requirePostalCode: false
                        };
                        this.cardIO.scan(options).then(response => {
                            console.log('Scan complete');

                            const {
                                cardType, cardNumber, redactedCardNumber,
                                expiryMonth, expiryYear, cvv, postalCode
                            } = response;

                            this.card = {
                                cardType,
                                cardNumber,
                                redactedCardNumber,
                                expiryMonth,
                                expiryYear,
                                cvv,
                                postalCode
                            };
                        });
                    }
                });
    }

    // Just to animate the fab
    fabGone = false;

    ionViewWillEnter() {
        this.fabGone = false;
    }

    ionViewWillLeave() {
        this.fabGone = true;
    }

    pay() {
        if (this.card.cardNumber === '' || this.card.cvv === '' || this.card.expiryYear === '' || this.card.expiryMonth === '') {
            alert('Please enter card details');
            return false;
        }
        const body = {
            amount: this.deposit.amount,
            id: this.deposit.id,
            idDepositPeriod: this.deposit.idDepositPeriod
        };
        this.rest.addDeposit(body).subscribe(
            (result: any) => {
                if (result.status.code > 200) {
                    if (result.status.code === 218) {
                        alert('Please enter ID Card Details');
                        this.navCtrl.setRoot('account');
                    }
                    return false;
                }
                this.navCtrl.setRoot('pay-success');
            });
    }

}
