import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentPage } from './payment';
import {CardIO} from "@ionic-native/card-io";
import {RestProvider} from "../../providers/rest/rest";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";

@NgModule({
  declarations: [
    PaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentPage),
  ],
  providers: [
    CardIO,
      RestProvider,
      HttpClientProvider,
      UtilProvider
  ]
})
export class PaymentPageModule {}
