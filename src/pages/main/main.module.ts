import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MainPage} from './main';
import {RestProvider} from "../../providers/rest/rest";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";
import {FabButtonPage} from "../fab-button/fab-button";
import {SharedModule} from "../../shared-module/shared.module";

@NgModule({
    declarations: [
        MainPage
    ],
    imports: [
        IonicPageModule.forChild(MainPage),
        SharedModule
    ],
    providers: [RestProvider, HttpClientProvider, UtilProvider]
})
export class MainPageModule {
}
