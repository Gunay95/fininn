import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import chartJs from 'chart.js';
import {RestProvider} from "../../providers/rest/rest";
import {Deposit} from "../../models/deposit";

import * as moment from 'moment';
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
    name: 'main'
})
@Component({
    selector: 'page-main',
    templateUrl: 'main.html',
})
export class MainPage {
    @ViewChild('lineCanvas') lineCanvas;
    deposites: Deposit[] = [];

    lineChart: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private rest: RestProvider, private util: UtilProvider) {
    }

  getDepositList() {
    this.util.showLoading();
    this.rest.getDepositList().subscribe(
      (result: any) => {
        this.util.hideLoading();
        if (result.status.code > 200) {
          this.util.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        this.deposites = result.data;
        setTimeout(() => {
          this.generateCharts();
        });
      }, (error) => {
        this.util.hideLoading();
      });
  }

    ionViewDidLoad() {
        this.getDepositList();
        setTimeout(() => {
            this.registerChartPlugin();
        }, 250);
        console.log('ionViewDidLoad MainPage');
    }

    generateCharts() {
        console.log(this.deposites)
        for (const deposit of this.deposites) {
            const total = deposit.amount * deposit.depositPeriodResponse.percent / 100;
            const monthlyInterest = deposit.amount * (deposit.depositPeriodResponse.percent / deposit.depositPeriodResponse.period) / 100;
            const months = moment().diff(moment(deposit.insertDate), 'months', true);
            const left = monthlyInterest * months;
            console.log(total, left);
            const data = {
                datasets: [{
                    data: [left.toFixed(1), (total - left).toFixed(1)],
                    backgroundColor: ['#36a2eb', '#cecece']
                }],
                labels: [
                    'Reached',
                    'Left'
                ]
            };
            const options = {
                elements: {
                    center: {
                        text: ((left * 100) / total).toFixed(1) + '%',
                        color: '#36a2eb', // Default is #000000
                        fontStyle: 'Arial', // Default is Arial
                        sidePadding: 20 // Defualt is 20 (as a percentage)
                    }
                }
            };
            this.getChart(document.getElementById('deposit-' + deposit.id), 'doughnut', data, options);
        }
    }

    depositClick(id) {
        this.navCtrl.setRoot('trans-list', {depositId: id})
    }

    getChart(context, chartType, data, options?) {
        return new chartJs(context, {
            data,
            options,
            type: chartType,
        });
    }

    registerChartPlugin() {
        chartJs.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;

                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
    }
}
