import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DepositCalculatorPage} from './deposit-calculator';
import {SharedModule} from "../../shared-module/shared.module";

@NgModule({
    declarations: [
        DepositCalculatorPage
    ],
    imports: [
        IonicPageModule.forChild(DepositCalculatorPage),
        SharedModule
    ],
})
export class DepositCalculatorPageModule {
}
