import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the DepositCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'deposit-calculator'
})
@Component({
  selector: 'page-deposit-calculator',
  templateUrl: 'deposit-calculator.html',
})
export class DepositCalculatorPage {
  amount = 100;
  total = 100;
  month = 12;
  currency = 'AZN';
  percentage = {
    '12': {
      azn: 7.0,
      usd: 0.1
    },
    '18': {
      azn: 7.5,
      usd: 0
    },
    '24': {
      azn: 8.0,
      usd: 0.2
    },
    '30': {
      azn: 8.5,
      usd: 0.3
    },
    '36': {
      azn: 9.0,
      usd: 0.5
    }
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.calculate();
  }

  calculate() {
    this.total = this.amount + this.amount * this.percentage[this.month.toString()][this.currency.toLowerCase()] / 100
  }

}
