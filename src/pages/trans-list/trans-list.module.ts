import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TransListPage} from './trans-list';
import {SharedModule} from "../../shared-module/shared.module";
import {RestProvider} from "../../providers/rest/rest";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";

@NgModule({
    declarations: [
        TransListPage
    ],
    imports: [
        IonicPageModule.forChild(TransListPage),
        SharedModule
    ],
  providers: [RestProvider, HttpClientProvider, UtilProvider]
})
export class TransListPageModule {
}
