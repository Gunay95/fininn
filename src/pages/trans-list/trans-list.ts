import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
  name: 'trans-list'
})
@Component({
  selector: 'page-trans-list',
  templateUrl: 'trans-list.html',
})
export class TransListPage {
  transactionList: any;
  groupedTransactions;

  constructor(public navCtrl: NavController, public navParams: NavParams, private rest: RestProvider, private util: UtilProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransListPage');
  }

  ionViewWillEnter() {
    if (this.navParams.get('depositId')) {
      const depositId = this.navParams.get('depositId');
      this.getDepositList(depositId)
    } else {
      this.getDepositList(0);
    }
  }

  getDepositList(id) {
    this.util.showLoading();
    this.rest.getTransactionList(id).subscribe(
      (result: any) => {
        this.util.hideLoading();
        if (result.status.code > 200) {
          this.util.presentToast('bottom', result.status.message, 3000);
          return false;
        }
        this.transactionList = result.data;
        this.generateTransactionGroup()
      });
  }

  generateTransactionGroup() {
    const deposit = [];
    for (const transaction of this.transactionList) {
      const has = deposit.find( (d) => d.title === 'DT-' + (1000 +transaction.idDeposit));
      if (has) {
        has.transactions.push(transaction);
      } else {
        deposit.push({title: 'DT-' + (1000 +transaction.idDeposit), transactions: [transaction]})
      }
    }
    this.groupedTransactions = deposit
  }

}
