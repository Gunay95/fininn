import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user";
import {SecurityProvider} from "../../providers/security/security";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {UtilProvider} from "../../providers/util/util";


@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  user: User = new User();
  usernameError = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private securityService: SecurityProvider, private httpClientService: HttpClientProvider, private utilService: UtilProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern(this.user.patterns.username)])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  get controls() {
    return this.loginForm.controls;
  }

  loginClick() {
    this.loginForm = this.formBuilder.group({
      username: [this.user.username, Validators.compose([Validators.required, Validators.pattern(this.user.patterns.username)])],
      password: [this.user.password, Validators.compose([Validators.required])],
    });
    console.log(this.loginForm.invalid);
    if (this.loginForm.invalid) {
      return;
    }
    this.utilService.showLoading();
    this.securityService.auth(this.user).subscribe(
      data => {
        const securityResponse: any = data;
        this.utilService.hideLoading();
        if (securityResponse.status.code > 200) {
          this.utilService.presentToast('bottom', securityResponse.status.message, 3000);
          return false;
        }
        this.navCtrl.setRoot('main');
        this.httpClientService.setAccessToken(securityResponse.data.token);
      }, (error) => {
        this.utilService.hideLoading();
      });
  }

  usernameFocusOut($event) {
    this.usernameError = !this.utilService.checkValueByPattern(this.user.username, this.user.patterns.username);
  }

  closeLoginPage() {
    this.navCtrl.pop()
  }

  signup() {
    this.navCtrl.push('register');
  }

}
