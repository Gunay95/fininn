#Finİn app
Bu tətbiq banka getmədən insanlara online şəkildə depozit hesabı yaratmaq imkanı verir. Mobil tətbiq həm ios həm də android üçün hazırlanmışdır.
Daha ətrafli təqdimatda danışılacaq.

##Funsionallıqlar
1. Qeydiyyatdan keçmək və daxil olmaq funksiyası
2. Onlayn şəkildə kartla depozit yatırmaq funksiyası
3. Bir istifadəçi üçün bir neçə depozit yaratmaq imkanı
4. Depozit hesabların qrafiklərlə əks olunması
5. Hər bir depozit hesaba aid tranzaksiyaların tarixçəsi
6. Depozitin ödənişi zamanı sürətli kart oxuma funksiyası
7. Depozitdən gəlirin hesablanması üçün xüsusi kalkulyator imkanı
8. İstifadəçinin hesab məlumatlarının göstərilməsi və idarə olunması imkanı.
9. Popup menu vasitəsilə tətbiqdən çıxış imkanı.


###İşləmə prinsipləri
1. Qeydiyyatdan keçmək və daxil olmaq funksiyası
	
	Qeydiyyatdan keçmək üçün istifadəçidən name, surname,  email,  phone(optional), password melumatları tələb olnur. Xanalara Validation - lar tətbiq  			olmuşdur.
	Daxil ol zamanı istifadəçi email və passwordu daxil etməlidir. Xanalara Validation - lar tətbiq olmuşdur.

2. Onlayn şəkildə kartla depozit yatırmaq funksiyası

	Bu səhifə popup menu da 2ci düymənin click-i zamanı açılır. Daxil olaraq verilmiş seçimləri doldurmaq lazımdır. İlk öncə Seçimlərə əsasən istifadəçiyə aylıq 		geliri, seçdiyi müddət ərzindəki cəmi gəliri və depozitlə birgə son məbləği göstərir. Daha sonra istifadəçi nəticə ilə razılaşarsa ekranın yuxarı sağ küncündəki 		NEXT düyməsinə click etməlidir. Növbəti açılan səhifədə scan üçün düymə yerləşdirilmişdir. Depoziti yerləşdirmək üçün lazım olan kartı scan edərək kart 		üzərindəki məlumatlar avtomatik xanalara doldurulur. Daha sonra bu səhifədə Pay düyməsinə click etməklə uğurlu və ya uğursuz səhifəsi 	açılacaqdır. Bu 		səhifədən popup menu vasitəsilə digər 	səhifələrə keçmək olar. Qeyd edim ki, istifadəçi Account sehifəsində fin code ve serial number melumatlarını daxil 		etməyibsə depozit yerləşdirə bilməz.

3. Bir istifadəçi üçün bir neçə depozit yaratmaq imkan 

	2 - ci funksionallığı təkrar tətbiq etməklə depozit sayını artırmaq mümkündür.

4. Depozit hesabların qrafiklərlə əks olunması

      Mobil tətbiqə daxil olarkən ilk olaraq açılan səhifədə qrafikləri görmək olar. Popup menu da isə HOME düyməsinə click etməklə keçmək olar. Əgər istifadəçi 		depozit yatırmayıbsa heç bir qrafik əks olunmur. 

5. Hər bir depozit hesaba aid tranzaksiyaların tarixçəsi

	Bu sehifəyə  popup menudan Tranzaksiya düyməsinə click etməklə keçid etmək olar. Açılan səhifədə istifadəçiyə aid olan tranzaksiyaların depozit hesabları 		üzrə qruplaşması göstərilmişdir.  Eyni zamanda depozitlərin siyahısından hansısa bir depozitə click etməklə həmin depozitə aid olan tranzaksiyalarln siyahısını 	görmək olar.

6. Depozitin ödənişi zamanı sürətli kart oxuma funksiyası

	2-ci funksionallığın tərkibində qeyd olunmuşdur.

7. Depozitdən gəlirin hesablanması üçün xüsusi kalkulyator imkanı

	Bu səhifəyə popup menudan Calc düyməsinə click etməklə keçmək olar.  Burada istifadəçi müxtəlif seçimlər etməklə depozitdən gelirini asanlıqla hesablaya 		bilər.

8. İstifadəçinin hesab məlumatlarının göstərilməsi və idarə olunması imkanı

	Bu sehifəyə popup menu dan Account düyməsinə click etməklə keçmək olar. Burada istifadəçi name, surname, phone, fin code, serial number melumatlarını 		mütləq daxil etmelidir. 







##Çatdıra bilmədiklərimiz
1. Müştərinin gelirinin müştərinin hesabına onlayn köçürülməsi
2. Hər əməliyyatda otp kodun istənilməsi
3. Email təsdiqi


##İstifadə etdiyimiz texnologiyalar
1. Ionic 3
2. Angular 4
3. Typescript
4. Html5, Css3, Sass
5. Cordova




##Komandalar

İlk növbədə Node.js yüklənməlidir. https://nodejs.org

Ionic, Cordova yükləmək üçün
```
# npm install -g ionic, cordova
```

Dependencyləri yükləmək üçün
```
# npm install
```

Browserdə run etmək üçün (native kamera skan plugini işləməyəcək)
```
# ionic serve
```

Android və ios-da run etmək üçün
```
# ionic cordova platform add android/ios
# ionic cordova run android/ios
```

